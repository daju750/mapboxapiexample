import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-zoom-range',
  templateUrl: './zoom-range.component.html',
  styleUrls: ['./zoom-range.component.css']
})
export class ZoomRangeComponent implements AfterViewInit{
  
  @ViewChild('mapa') divMapa!: ElementRef;
  mapa!: mapboxgl.Map;
  zoomLevel:number = 17;

  ngAfterViewInit(): void {
      
      this.mapa = new mapboxgl.Map({
      container: this.divMapa.nativeElement,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-90.78139577066057,14.300054741518595 ],
      zoom: this.zoomLevel
    });

    this.mapa.on('zoom',(evnt)=>{
      this.zoomLevel = this.mapa.getZoom();
    })

    this.mapa.on('zoomend',(envt)=>{
      if(this.mapa.getZoom()>18){
        this.mapa.zoomTo(18);
      }
    })

  }

  zoomCambio(valor: string){
    this.mapa.zoomTo(Number(valor));
  }

  zoomOut(){this.mapa.zoomOut();}

  zoomIn(){this.mapa.zoomIn();}

}
